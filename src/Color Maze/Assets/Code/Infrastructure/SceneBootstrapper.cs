﻿using Code.Extension;
using ColorWall;
using ColorWall.Code;
using UnityEngine;

namespace Code.Infrastructure
{
  public class SceneBootstrapper : MonoBehaviour
  {
    private LayerCollisionService _layerCollisionService;

    private void Start()
    {
      CollectServices();
      InitWalls();
    }

    private void CollectServices()
    {
      _layerCollisionService = FindObjectOfType<LayerCollisionService>();
    }

    private void InitWalls()
    {
      Wall[] walls = FindObjectsOfType<Wall>();
      walls.ForEach(wall => wall.Init(_layerCollisionService));
    }
  }
}
