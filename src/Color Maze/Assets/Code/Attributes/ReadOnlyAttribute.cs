using UnityEngine;

namespace Code.Attributes
{
  public class ReadOnlyAttribute : PropertyAttribute { }
}
