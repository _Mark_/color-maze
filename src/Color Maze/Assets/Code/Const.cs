﻿namespace Code
{
  public static class Const
  {
    public static class Tag
    {
      public const string Player = "Player";
    }

    public static class Layer
    {
      public const string Player = "Player";
      public const string Impenetrable = "Impenetrable";
      public const string RedWall = "Red wall";
      public const string GreenWall = "Green wall";
      public const string WallDetector = "Wall detector";
    }
  }
}
