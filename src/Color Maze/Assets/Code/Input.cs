﻿using System;
using UnityEngine;

namespace Code
{
  public class Input : MonoBehaviour
  {
    private const KeyCode _forward = KeyCode.W;
    private const KeyCode _back = KeyCode.S;
    private const KeyCode _left = KeyCode.A;
    private const KeyCode _right = KeyCode.D;

    public event Action Pressed;
    public event Action ForwardPressed;
    public event Action BackPressed;
    public event Action RightPressed;
    public event Action LeftPressed;

    public event Action Cancelled;
    public event Action ForwardCancelled;
    public event Action BackCancelled;
    public event Action RightCancelled;
    public event Action LeftCancelled;

    private void Awake()
    {
      ForwardPressed += Pressed;
      BackPressed += Pressed;
      LeftPressed += Pressed;
      RightPressed += Pressed;

      ForwardCancelled += Cancelled;
      BackCancelled += Cancelled;
      RightCancelled += Cancelled;
      LeftCancelled += Cancelled;
    }

    private void Update()
    {
      if (UnityEngine.Input.GetKeyDown(_forward)) ForwardPressed?.Invoke();
      if (UnityEngine.Input.GetKeyDown(_back)) BackPressed?.Invoke();
      if (UnityEngine.Input.GetKeyDown(_left)) LeftPressed?.Invoke();
      if (UnityEngine.Input.GetKeyDown(_right)) RightPressed?.Invoke();

      if (UnityEngine.Input.GetKeyUp(_forward)) ForwardCancelled?.Invoke();
      if (UnityEngine.Input.GetKeyUp(_back)) BackCancelled?.Invoke();
      if (UnityEngine.Input.GetKeyUp(_left)) LeftCancelled?.Invoke();
      if (UnityEngine.Input.GetKeyUp(_right)) RightCancelled?.Invoke();
    }
  }
}