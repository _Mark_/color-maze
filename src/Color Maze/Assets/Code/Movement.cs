using UnityEngine;

namespace Code
{
  public class Movement : MonoBehaviour
  {
    [Header("Settings")]
    [SerializeField] private float _speed = 5;
    [Space]
    [SerializeField] private Input _input;
    [Space]
    [SerializeField] private Transform _directionPivot;
    [Space]
    [SerializeField] private Rigidbody _body;

    private Vector3 _movementDirection;

    private void Start()
    {
      Initialize();
    }

    private void Update()
    {
      _body.velocity = _movementDirection.normalized * _speed;
    }

    private void Initialize()
    {
      _input.ForwardPressed += OnForwardPressed;
      _input.BackPressed += OnBackPressed;
      _input.LeftPressed += OnLeftPressed;
      _input.RightPressed += OnRightPressed;

      _input.ForwardCancelled += OnForwardCancelled;
      _input.BackCancelled += OnBackCancelled;
      _input.LeftCancelled += OnLeftCancelled;
      _input.RightCancelled += OnRightCancelled;
    }

    private void OnForwardPressed()
    {
      _movementDirection += _directionPivot.forward;
    }

    private void OnForwardCancelled()
    {
      _movementDirection -= _directionPivot.forward;
    }

    private void OnBackPressed()
    {
      _movementDirection += -_directionPivot.forward;
    }

    private void OnBackCancelled()
    {
      _movementDirection -= -_directionPivot.forward;
    }

    private void OnLeftPressed()
    {
      _movementDirection += -_directionPivot.right;
    }

    private void OnLeftCancelled()
    {
      _movementDirection -= -_directionPivot.right;
    }

    private void OnRightPressed()
    {
      _movementDirection += _directionPivot.right;
    }

    private void OnRightCancelled()
    {
      _movementDirection -= _directionPivot.right;
    }
  }
}
