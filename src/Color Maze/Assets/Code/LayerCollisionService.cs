﻿using ColorWall.Code;
using UnityEngine;

namespace Code
{
  public class LayerCollisionService : MonoBehaviour
  {
    public void EnableCollisionWithLayer(LayerMask wallLayer, IWallConnection obj)
    {
      Physics.IgnoreLayerCollision(obj.ClosedLayer, obj.Layer);
      Physics.IgnoreLayerCollision(wallLayer, obj.Layer, false);
      obj.ClosedLayer = wallLayer;
    }
  }
}
