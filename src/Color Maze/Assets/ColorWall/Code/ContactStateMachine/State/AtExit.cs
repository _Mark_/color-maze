﻿namespace ColorWall.Code.ContactStateMachine.State
{
  public class AtExit : ContactWallState
  {
    public AtExit(StateMachine stateMachine, Detection[] detections) : base(stateMachine, detections) { }

    public override void Execute(DetectionAction detectionAction, Detection detection)
    {
      switch (detectionAction)
      {
        case DetectionAction.Entered:
          StateMachine.MoveTo(StateType.Intermediate);
          break;

        case DetectionAction.Exited:
          StateMachine.OnWallOvercome();
          StateMachine.MoveTo(StateType.Outside);
          break;
      }
    }
  }
}
