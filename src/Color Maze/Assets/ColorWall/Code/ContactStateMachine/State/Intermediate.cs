﻿namespace ColorWall.Code.ContactStateMachine.State
{
  public class Intermediate : ContactWallState
  {
    public Intermediate(StateMachine stateMachine, Detection[] detections) : base(stateMachine, detections) { }

    public override void Execute(DetectionAction detectionAction, Detection detection)
    {
      if (detection == Detections[0])
      {
        StateMachine.MoveTo(StateType.AtExit);
        return;
      }

      Detections[1] = Detection.None;
      StateMachine.MoveTo(StateType.AtEnter);
    }
  }
}
