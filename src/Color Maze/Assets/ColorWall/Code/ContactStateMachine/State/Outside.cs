﻿namespace ColorWall.Code.ContactStateMachine.State
{
  public class Outside : ContactWallState
  {
    public Outside(StateMachine stateMachine, Detection[] detections) : base(stateMachine, detections) { }

    protected override void OnEnter()
    {
      for (int i = 0; i < Detections.Length; i++)
      {
        Detections[i] = 0;
      }
      StateMachine.LeaveWall();
    }

    public override void Execute(DetectionAction detectionAction, Detection detection)
    {
      if (detectionAction == DetectionAction.Entered)
      {
        Detections[0] = detection;
        StateMachine.MoveTo(StateType.AtEnter);
      }
    }
  }
}
