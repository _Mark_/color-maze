﻿namespace ColorWall.Code.ContactStateMachine.State
{
  public class AtEnter : ContactWallState
  {
    public AtEnter(StateMachine stateMachine, Detection[] detections) : base(stateMachine, detections) { }

    protected override void OnEnter()
    {
      StateMachine.TouchToWall(StateMachine.WallConnection);
    }

    public override void Execute(DetectionAction detectionAction, Detection detection)
    {
      switch (detectionAction)
      {
        case DetectionAction.Exited:
          StateMachine.MoveTo(StateType.Outside);
          break;

        case DetectionAction.Entered:
          Detections[1] = detection;
          StateMachine.MoveTo(StateType.Intermediate);
          break;
      }
    }
  }
}
