namespace ColorWall.Code.ContactStateMachine.State
{
  public abstract class ContactWallState
  {
    protected readonly StateMachine StateMachine;
    protected readonly Detection[] Detections;

    protected ContactWallState(StateMachine stateMachine, Detection[] detections)
    {
      StateMachine = stateMachine;
      Detections = detections;
    }

    public void Enter()
    {
      OnEnter();
    }

    public void Exit()
    {
      OnExit();
    }

    public abstract void Execute(DetectionAction detectionAction, Detection detection);
    protected virtual void OnEnter() { }
    protected virtual void OnExit() { }
  }
}
