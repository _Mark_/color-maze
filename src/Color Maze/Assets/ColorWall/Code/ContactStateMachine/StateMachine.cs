﻿using System.Collections.Generic;
using ColorWall.Code.ContactStateMachine.State;

namespace ColorWall.Code.ContactStateMachine
{
  public class StateMachine
  {
    private ContactWallState _current;
    private readonly Wall _wall;
    private readonly Dictionary<StateType, ContactWallState> _states;

    public StateMachine(Detection[] detections, Wall wall)
    {
      _states = new Dictionary<StateType, ContactWallState>()
      {
        { StateType.Outside, new Outside(this, detections) },
        { StateType.AtEnter, new AtEnter(this, detections) },
        { StateType.Intermediate, new Intermediate(this, detections) },
        { StateType.AtExit, new AtExit(this, detections) },
      };

      _wall = wall;

      _current = _states[StateType.Outside];
      _current.Enter();
    }

    public IWallConnection WallConnection { get; set; }

    public void Execute(DetectionAction detectionAction, Detection detection)
    {
      _current.Execute(detectionAction, detection);
    }

    public void MoveTo(StateType state)
    {
      _current.Exit();
      _current = _states[state];
      _current.Enter();
    }

    public void OnWallOvercome()
    {
      _wall.OnTheWallOvercome();
    }

    public void LeaveWall()
    {
      _wall.LeaveWall();
    }

    public void TouchToWall(IWallConnection wallConnection)
    {
      _wall.TouchToWall(wallConnection);
    }
  }
}
