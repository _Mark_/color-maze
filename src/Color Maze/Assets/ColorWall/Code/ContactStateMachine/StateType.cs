﻿namespace ColorWall.Code.ContactStateMachine
{
  public enum StateType
  {
    Outside,
    AtEnter,
    Intermediate,
    AtExit,
  }
}
