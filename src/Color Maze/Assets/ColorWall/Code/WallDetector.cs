﻿using System;
using UnityEngine;

namespace ColorWall.Code
{
  public class WallDetector : MonoBehaviour
  {
    [SerializeField] private Wall _wall;

    public event Action<IWallConnection> PlayerEntered;
    public event Action<IWallConnection> PlayerExited;

    private void OnTriggerEnter(Collider other)
    {
      if (other.TryGetComponent<IWallConnection>(out var wallConnection))
        PlayerEntered?.Invoke(wallConnection);
    }

    private void OnTriggerExit(Collider other)
    {
      if (other.TryGetComponent<IWallConnection>(out var wallConnection))
        PlayerExited?.Invoke(wallConnection);
    }
  }
}
