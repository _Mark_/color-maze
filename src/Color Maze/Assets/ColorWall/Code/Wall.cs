﻿using Code;
using Code.Attributes;
using ColorWall.Code.ContactStateMachine;
using UnityEngine;

namespace ColorWall.Code
{
  public class Wall : MonoBehaviour
  {
    [SerializeField] private WallDetector firstWall;
    [SerializeField] private WallDetector secondWall;
    [SerializeField] private MeshRenderer _meshRenderer;
    [ReadOnly, SerializeField] private Detection[] _detections = new Detection[2];

    private Color _color;
    private bool _isCrossedTheHalf;
    private StateMachine _stateMachine;
    private IWallConnection _currentWallConnection;
    private LayerCollisionService _layerCollisionController;

    private void OnDestroy()
    {
      firstWall.PlayerEntered -= OnPlayerEnteredIntoFirstWall;
      firstWall.PlayerExited -= OnPlayerExitedIntoFirstWall;
      secondWall.PlayerEntered -= OnPlayerEnteredIntoSecondWall;
      secondWall.PlayerExited -= OnPlayerExitedIntoSecondWall;
    }

    private void Awake()
    {
      firstWall.PlayerEntered += OnPlayerEnteredIntoFirstWall;
      firstWall.PlayerExited += OnPlayerExitedIntoFirstWall;
      secondWall.PlayerEntered += OnPlayerEnteredIntoSecondWall;
      secondWall.PlayerExited += OnPlayerExitedIntoSecondWall;

      _color = _meshRenderer.material.color;
      _stateMachine = new StateMachine(_detections, this);
    }

    public void Init(LayerCollisionService layerCollisionController)
    {
      _layerCollisionController = layerCollisionController;
    }

    public void OnTheWallOvercome()
    {
      _layerCollisionController.EnableCollisionWithLayer(gameObject.layer, _currentWallConnection);
      _currentWallConnection.ChangeColor(_color);
      _currentWallConnection = null;
    }

    public void LeaveWall()
    {
      _currentWallConnection = null;
    }

    public void TouchToWall(IWallConnection wallConnection)
    {
      _currentWallConnection = wallConnection;
    }

    private void OnPlayerEnteredIntoFirstWall(IWallConnection wallConnection)
    {
      _stateMachine.WallConnection = wallConnection;
      _stateMachine.Execute(DetectionAction.Entered, Detection.First);
    }

    private void OnPlayerExitedIntoFirstWall(IWallConnection wallConnection)
    {
      _stateMachine.Execute(DetectionAction.Exited, Detection.First);
    }

    private void OnPlayerEnteredIntoSecondWall(IWallConnection wallConnection)
    {
      _stateMachine.WallConnection = wallConnection;
      _stateMachine.Execute(DetectionAction.Entered, Detection.Second);
    }

    private void OnPlayerExitedIntoSecondWall(IWallConnection wallConnection)
    {
      _stateMachine.Execute(DetectionAction.Exited, Detection.Second);
    }
  }
}
