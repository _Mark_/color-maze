﻿using UnityEngine;

namespace ColorWall.Code
{
  public class WallConnection : MonoBehaviour, IWallConnection
  {
    private Material _material;
    
    public LayerMask Layer => gameObject.layer;

    public LayerMask ClosedLayer { get; set; }

    private void Awake()
    {
      _material = GetComponent<MeshRenderer>().material;
    }

    public void ChangeColor(Color newColor)
    {
      _material.color = newColor;
    }
  }
}
