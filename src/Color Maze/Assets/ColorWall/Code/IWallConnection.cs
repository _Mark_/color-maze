﻿using UnityEngine;

namespace ColorWall.Code
{
  public interface IWallConnection
  {
    LayerMask Layer { get; }
    LayerMask ClosedLayer { get; set; }
    
    void ChangeColor(Color newColor);
  }
}
