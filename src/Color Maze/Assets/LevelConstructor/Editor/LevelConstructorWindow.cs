﻿using System.Collections.Generic;
using System.Linq;
using System.Web.UI.WebControls;
using Editor;
using UnityEditor;
using UnityEngine;
using UnityEngine.UIElements;
using Image = UnityEngine.UIElements.Image;
using Label = UnityEngine.UIElements.Label;
using ListView = UnityEngine.UIElements.ListView;

namespace LevelConstructor.Editor
{
  public class LevelConstructorWindow : EditorWindow
  {
    private static LevelConstructorWindow _window;

    private VisualElement _rightPane;

    [UnityEditor.MenuItem("🛠/Level constructor window _F1")]
    public static void ShowWindow()
    {
      if (HasOpenInstances<LevelConstructorWindow>())
      {
        _window.Close();
        return;
      }

      _window = GetWindow<LevelConstructorWindow>(nameof(LevelConstructorWindow));
      // _window.minSize = new Vector2(800, 600);
    }

    private void CreateGUI()
    {
      var allSprites = FindAllSprites();

      var splitView = new TwoPaneSplitView(0, 250, TwoPaneSplitViewOrientation.Horizontal);
      rootVisualElement.Add(splitView);


      var listView = new ListView(allSprites, 50, () =>
        {
          VisualElement result = new VisualElement();
          result.Add(new Label());
          result.Add(new Image());
          return result;
        },
        (visualElement, index) =>
        {
          (visualElement.Children().First(child => child is Label) as Label).text = allSprites[index].name;
          var image = (visualElement.Children().First(child => child is Image) as Image);
          image.scaleMode = ScaleMode.ScaleToFit;
          image.sprite = allSprites[index];
        });
      listView.onSelectionChange += OnListViewSelectionChange;
      splitView.Add(listView);


      // _rightPane = new VisualElement();
      _rightPane = new ScrollView(ScrollViewMode.VerticalAndHorizontal);
      splitView.Add(_rightPane);
    }

    private static List<Sprite> FindAllSprites()
    {
      var allSpritesGuids = AssetDatabase.FindAssets(Const.Type.Sprite);
      var allSprites = new List<Sprite>();

      foreach (var guid in allSpritesGuids)
      {
        var path = AssetDatabase.GUIDToAssetPath(guid);
        var asset = AssetDatabase.LoadAssetAtPath<Sprite>(path);
        allSprites.Add(asset);
      }

      return allSprites;
    }

    private void OnListViewSelectionChange(IEnumerable<object> selectedItems)
    {
      _rightPane.Clear();
      var sprite = selectedItems.First() as Sprite;
      if (sprite is null) return;

      var image = new Image
      {
        scaleMode = ScaleMode.ScaleToFit,
        sprite = sprite
      };

      _rightPane.Add(image);
    }
  }
}