﻿using System.Linq;
using UnityEditor;
using UnityEditor.EditorTools;
using UnityEditor.SceneManagement;
using UnityEngine;

namespace LevelConstructor.Editor
{
  [EditorTool("Snapping tool", typeof(Snap))]
  public class SnappingTool : EditorTool
  {
    [SerializeField] private Texture2D _toolbarIcon;

    private float rectSize = 500;
    private float _snapTriggerDistance = 0.5f;
    private Transform _targetTransform;
    private SnapPoint[] _allPoints;
    private SnapPoint[] _ownPoints;

    public override void OnActivated()
    {
      _targetTransform = ((Snap) target).transform;
      _ownPoints = _targetTransform.GetComponentsInChildren<SnapPoint>();

      var prefabStage = PrefabStageUtility.GetPrefabStage(_targetTransform.gameObject);
      _allPoints = prefabStage ? prefabStage.prefabContentsRoot.GetComponentsInChildren<SnapPoint>() : FindObjectsOfType<SnapPoint>();
    }

    public override GUIContent toolbarIcon =>
      new GUIContent()
      {
        image = _toolbarIcon,
        text = "Snapping tool",
        tooltip = "null",
      };

    public override void OnToolGUI(EditorWindow window)
    {
      SnapPoint currentPoint = null;
      Vector2 currentPointScreenPosition = Vector2.positiveInfinity;
      Vector2 mousePosition = Event.current.mousePosition;
      var camera = SceneView.lastActiveSceneView.camera;

      foreach (var point in _ownPoints)
      {
        Vector2 pointScreenPosition = camera.WorldToScreenPoint(point.Position);
        pointScreenPosition = new Vector2(pointScreenPosition.x, camera.pixelHeight - pointScreenPosition.y);
        var rect = new Rect(mousePosition.x - rectSize / 2, mousePosition.y - rectSize / 2, rectSize, rectSize);

        if (rect.Contains(pointScreenPosition))
        {
          if (currentPoint)
          {
            if (Vector2.Distance(currentPointScreenPosition, mousePosition) > Vector2.Distance(pointScreenPosition, mousePosition))
            {
              currentPoint = point;
              currentPointScreenPosition = pointScreenPosition;
            }
          }
          else
          {
            currentPoint = point;
            currentPointScreenPosition = pointScreenPosition;
          }
        }
      }

      if (currentPoint == null) return;

      EditorGUI.BeginChangeCheck();
      Vector3 newSnapPointPosition = Handles.PositionHandle(currentPoint.Position, Quaternion.identity);

      if (EditorGUI.EndChangeCheck())
      {
        Undo.RecordObjects(new[] { _targetTransform, target }, "Object move");
        MoveWithSnapping(currentPoint, newSnapPointPosition);
      }
    }

    private void MoveWithSnapping(SnapPoint currentPoint, Vector3 newSnapPointPosition)
    {
      var snapToPivot = _targetTransform.position - currentPoint.Position;
      SnapPoint bestSnapPoint = BestPoint(currentPoint, out var distanceToBestPoint);

      if (bestSnapPoint &&
          Vector3.Distance(newSnapPointPosition, bestSnapPoint.Position) > _snapTriggerDistance)
      {
        _targetTransform.position = newSnapPointPosition + snapToPivot;
        return;
      }

      if (distanceToBestPoint < _snapTriggerDistance)
        _targetTransform.position = bestSnapPoint.Position + snapToPivot;
      else
        _targetTransform.position = newSnapPointPosition + snapToPivot;
    }

    private SnapPoint BestPoint(SnapPoint currentPoint, out float distanceToBestPoint)
    {
      SnapPoint bestSnapPoint = null;
      distanceToBestPoint = float.PositiveInfinity;
      var points = _allPoints.Except(_ownPoints);

      foreach (var point in points)
      {
        var distance = Vector3.Distance(point.Position, currentPoint.Position);

        if (distance < distanceToBestPoint)
        {
          bestSnapPoint = point;
          distanceToBestPoint = distance;
        }
      }

      return bestSnapPoint;
    }
  }
}