using UnityEngine;

namespace LevelConstructor
{
    public class SnapPoint : MonoBehaviour
    {
        public Vector3 Position => transform.position;
        
        private void OnDrawGizmos()
        {
            Gizmos.color = Color.green;
            Gizmos.DrawSphere(transform.position, .2f);
        }
    }
}
